# Standard library.
import math
# NumPy.
import numpy as np


"""
	Gaussian_Map_Base

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Gaussian_Map_Base(n, r):
	# Create a crenellation approximately within [-r, r].

	# Base.
	return sorted([(1 - n + 2*i + np.random.normal(0, 1/2))*r/n for i in range(n)])


"""
	Gaussian_Map_G

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Gaussian_Map_G(n, r):
	# Create a crenellation approximately within [-r, r].

	# Polynomial global scale.
	return sorted([(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.normal(0, 1/2))*r/n for i in range(n)])


"""
	Gaussian_Map_L

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Gaussian_Map_L(n, r):
	# Create a crenellation approximately within [-r, r].

	# Polynomial local scale.
	return sorted([(1 - n + 2*i + np.random.normal(0, (1/2)*(4/5 + (n - 1)/(5*n))))*r/n for i in range(n)])


"""
	Gaussian_Map_GL

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Gaussian_Map_GL(n, r):
	# Create a crenellation approximately within [-r, r].

	# Polynomial global + local scale.
	return sorted([(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.normal(0, (1/2)*(4/5 + (n - 1)/(5*n))))*r/n for i in range(n)])
