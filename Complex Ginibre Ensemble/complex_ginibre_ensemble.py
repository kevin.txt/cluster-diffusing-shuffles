# Standard library.
import argparse
import csv
# tqdm.
from tqdm import tqdm
# NumPy.
import numpy as np
# SciPy.
import scipy.spatial as spatial


def main(args):
	with open("complex_ginibre_ensemble_eigenvalues.csv", "w", newline = "") as eig_file, open(f"complex_ginibre_ensemble_eigenvalue_KNN_dists.csv", "w", newline = "") as knn_file:
		# Eigenvalue file.
		eig_writer = csv.writer(eig_file)
		eig_writer.writerow(["Real", "Complex", "Round"])

		# Eigenvalue KNN file.
		knn_writer = csv.writer(knn_file)
		knn_writer.writerow(["K", "Distance to KNN", "Round"])

		for round in tqdm(range(1, args.r + 1)):
			# Create a matrix from the Complex Ginibre Ensemble.
			m = np.ndarray((args.n, args.n), complex)

			# Fill the matrix with complex standard normal r.v.s. Coarse scale normalize it so the eigenvalues sit on the complex unit disk.
			# https://en.wikipedia.org/wiki/Complex_normal_distribution#Complex_standard_normal_random_variable
			for i in range(0, args.n):
				for j in range(0, args.n):
					m[i][j] = complex(np.random.normal(0, 1/np.sqrt(2*args.n)), np.random.normal(0, 1/np.sqrt(2*args.n)))

			# Spectral decompose.
			eigvals = np.linalg.eigvals(m)

			# Turn the complex eigenvalues into pairs of floats.
			eigvals = eigvals.view(float).reshape(eigvals.shape + (2,))

			# Build a k-d tree of the eigenvalues.
			kdtree = spatial.KDTree(eigvals)

			# Process eigenvalues.
			for eigval in eigvals:
				# Write out the eigenvalue.
				eig_writer.writerow([eigval[0], eigval[1], round])

				# Find the k-nearest neighbor distances.
				knn_dists = kdtree.query(eigval, k = args.n, p = 2)[0][1:]

				# Write out the k-nearest-neighbor distances.
				for k, dist in enumerate(knn_dists):
					knn_writer.writerow([k + 1, dist, round])



"""
	CLI.
"""
if __name__ == "__main__":
	# Argument parser.
	parser = argparse.ArgumentParser(description="Random Complex Matrix Eigenvalues and Nearest Neighbor Distributions")

	# Dimension of the random complex square matrix to spectral decompose.
	parser.add_argument("-n", type = int, required = True, help = "Dimension of the Complex Ginibre Ensemble samples.", dest = "n")

	# Number of rounds.
	parser.add_argument("-r", type = int, required = False, help = "Number of rounds to sample the Complex Ginibre Ensemble.", dest = "r", default = 1)

	# Pass arguments.
	main(parser.parse_args())