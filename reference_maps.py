# Standard library.
import math
import random

"""
	Polacek_Map

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the position.

		(float)w
			Proportion of the width between the evenly space songs for varying the evenly spaced songs.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Polacek_Map(n, r, w):
	# Step 1: Spread out evenly over [0, 2r].
	positions = [(r + 2*r*float(i))/float(n) for i in range(n)]

	# Step 2: Vary randomly using uniform distribution.
	for i in range(len(positions)):
		positions[i] += random.uniform(-w*r/float(n), w*r/float(n))

	# Step 3: Random circular shift.
	shift = random.uniform(0, 2*r)
	for i in range(len(positions)):
		positions[i] = math.fmod(positions[i] + shift, 2*r)
		# Shift the positions to be on [-r, r].
		positions[i] -= r

	# Step 4: Sort the positions.
	positions.sort()

	# Return the positions.
	return positions


"""
	Balanced_Map

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the position.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Balanced_Map(n, r):
	# Step 1: Spread out evenly over [0, 2r].
	positions = [(r + 2*r*float(i))/float(n) for i in range(n)]

	# Step 2: Vary randomly using uniform distribution.
	for i in range(len(positions)):
		positions[i] += random.uniform(-r/float(n), r/float(n))
		# Shift the positions to be on [-r, r].
		positions[i] -= r

	# Step 3: Sort the positions.
	positions.sort()

	# Return the positions.
	return positions