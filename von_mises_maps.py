# Standard library.
import math
# NumPy.
import numpy as np


"""
	Von_Mises_Map_Base

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Von_Mises_Map_Base(n, r):
	# Create a crenellation approximately within [-r, r].

	# Base.
	return [(1 - n + 2*i + np.random.vonmises(0, math.pow(2/math.pi, 2))/math.pi)*r/n for i in range(n)]


"""
	Von_Mises_Map_Base_2

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Von_Mises_Map_Base_2(n, r):
	# Create a crenellation approximately within [-r, r].

	# κ such that σ = (1 - BesselI_1(κ)/BesselI_0(κ)) = 1/2.
	k = 2.36930117730843038470022095744951956629837304068436818126277796

	# Base.
	return [(1 - n + 2*i + np.random.vonmises(0, k)/math.pi)*r/n for i in range(n)]


"""
	Von_Mises_Map_G

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Von_Mises_Map_G(n, r):
	# Create a crenellation approximately within [-r, r].

	# Polynomial global scale.
	return [(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.vonmises(0, math.pow(2/math.pi, 2))/math.pi)*r/n for i in range(n)]


"""
	Von_Mises_Map_L

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Von_Mises_Map_L(n, r):
	# Create a crenellation approximately within [-r, r].

	# Polynomial local scale.
	return [(1 - n + 2*i + np.random.vonmises(0, math.pow(2/(math.pi*((4/5) + (n - 1)/(5*n))), 2))/math.pi)*r/n for i in range(n)]


"""
	Von_Mises_Map_GL

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Von_Mises_Map_GL(n, r):
	# Create a crenellation approximately within [-r, r].

	# Polynomial global + local scale.
	return [(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.vonmises(0, math.pow(2/(math.pi*((4/5) + (n - 1)/(5*n))), 2))/math.pi)*r/n for i in range(n)]