# Standard library.
import argparse
import csv
import math
import random
# tqdm.
from tqdm import tqdm
# CD shuffles.
import cluster_diffusing_shuffles as cds


def main(args):
	# Write out the partial alter statistics.
	with open("benchmark_partial_alter.csv", "w", newline = "") as file:
		# File.
		writer = csv.writer(file)
		writer.writerow(["Item", "Index", "Round", "Index Change"])

		# Initial song ordering.
		songs = [_ for _ in range(args.n)]
		random.shuffle(songs)

		# Previous song indices.
		prev_i = dict([(song, i) for i, song in enumerate(songs)])

		# Partial alter.
		for r in tqdm(range(args.r)):
			cds.Partial_Alter(songs)

			for i, song in enumerate(songs):
				# Find the index change.
				d_i = i - prev_i[song]
				# Write out the data.
				writer.writerow([song, i, r, d_i])
				# Update the previous index.
				prev_i[song] = i


"""
	CLI.
"""
if __name__ == "__main__":
	# Argument parser.
	parser = argparse.ArgumentParser(description="Benchmark: Partial Alter")

	# Number of songs to partial alter.
	parser.add_argument("-n", type = int, required = True, help = "Number of songs to partial alter.", dest = "n")

	# Number of rounds.
	parser.add_argument("-r", type = int, required = True, help = "Number of rounds.", dest = "r")

	# Pass arguments.
	main(parser.parse_args())