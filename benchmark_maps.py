# Standard library.
import argparse
import csv
import collections
from functools import partial
import math
import random
# tqdm.
from tqdm import tqdm
# CD shuffles.
import cluster_diffusing_shuffles as cds
# Polacek map.
import reference_maps as rm
# Gaussian maps.
import gaussian_maps as gm
# Von Mises maps.
import von_mises_maps as vmm


def main(args):
	# Tiny playlists.
	# Impulse: 2(5) = 10
	impulse_t = [[i for _ in range(2)] for i in range(5)]
	# Uniform: 1, ..., 4 = 10
	uniform_t = [[i for _ in range(i + 1)] for i in range(4)]
	# Binomial: 1, 2(2), 3 = 8
	binomial_t = [[i for _ in range(length)] for i, length in enumerate([1, 2, 2, 3])]
	# Zipf: 6(1 + 1/2 + 1/3) = 11
	zipf_t = [[i for _ in range(int(6/(i + 1)))] for i in range(3)]

	print("impulse_t")
	print([len(_) for _ in impulse_t], sum([len(_) for _ in impulse_t]))
	print("uniform_t")
	print([len(_) for _ in uniform_t], sum([len(_) for _ in uniform_t]))
	print("binomial_t")
	print([len(_) for _ in binomial_t], sum([len(_) for _ in binomial_t]))
	print("zipf_t")
	print([len(_) for _ in zipf_t], sum([len(_) for _ in zipf_t]))
	print()

	# Small playlists.
	# Impulse: 3(7) = 21
	impulse_s = [[i for _ in range(3)] for i in range(7)]
	# Uniform: 1, ..., 6 = 21
	uniform_s = [[i for _ in range(i + 1)] for i in range(6)]
	# Binomial: 1, 2(3), 3(3), 4 = 20
	binomial_s = [[i for _ in range(length)] for i, length in enumerate([1, 2, 2, 2, 3, 3, 3, 4])]
	# Zipf: 12(1 + 1/2 + 1/3 + 1/4) = 25
	zipf_s = [[i for _ in range(int(12/(i + 1)))] for i in range(4)]

	print("impulse_s")
	print([len(_) for _ in impulse_s], sum([len(_) for _ in impulse_s]))
	print("uniform_s")
	print([len(_) for _ in uniform_s], sum([len(_) for _ in uniform_s]))
	print("binomial_s")
	print([len(_) for _ in binomial_s], sum([len(_) for _ in binomial_s]))
	print("zipf_s")
	print([len(_) for _ in zipf_s], sum([len(_) for _ in zipf_s]))
	print()

	# Medium playlists.
	# Impulse: 5(11) = 55
	impulse_m = [[i for _ in range(5)] for i in range(11)]
	# Uniform: 1, ..., 10 = 55
	uniform_m = [[i for _ in range(i + 1)] for i in range(10)]
	# Binomial: 1, 2(4), 3(6), 4(4), 5 = 48
	binomial_m = [[i for _ in range(length)] for i, length in enumerate([1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5])]
	# Zipf: 24(1 + 1/2 + 1/3 + 1/4) = 50
	zipf_m = [[i for _ in range(int(24/(i + 1)))] for i in range(4)]

	print("impulse_m")
	print([len(_) for _ in impulse_m], sum([len(_) for _ in impulse_m]))
	print("uniform_m")
	print([len(_) for _ in uniform_m], sum([len(_) for _ in uniform_m]))
	print("binomial_m")
	print([len(_) for _ in binomial_m], sum([len(_) for _ in binomial_m]))
	print("zipf_m")
	print([len(_) for _ in zipf_m], sum([len(_) for _ in zipf_m]))
	print()

	# Large playlists.
	# Impulse: 8(17) = 136
	impulse_l = [[i for _ in range(8)] for i in range(17)]
	# Uniform: 1, ..., 16 = 136
	uniform_l = [[i for _ in range(i + 1)] for i in range(16)]
	# Binomial: 1, 2(5), 3(10), 4(10), 5(5), 6 = 112
	binomial_l = [[i for _ in range(length)] for i, length in enumerate([1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6])]
	# Zipf: 60(1 + 1/2 + 1/3 + 1/4 + 1/5) = 137
	zipf_l = [[i for _ in range(int(60/(i + 1)))] for i in range(5)]

	print("impulse_l")
	print([len(_) for _ in impulse_l], sum([len(_) for _ in impulse_l]))
	print("uniform_l")
	print([len(_) for _ in uniform_l], sum([len(_) for _ in uniform_l]))
	print("binomial_l")
	print([len(_) for _ in binomial_l], sum([len(_) for _ in binomial_l]))
	print("zipf_l")
	print([len(_) for _ in zipf_l], sum([len(_) for _ in zipf_l]))
	print()

	# Write out the CD shuffle statistics.
	with open("benchmark_maps_cluster_sizes.csv", "w", newline = "") as cluster_sizes_file, open("benchmark_maps_cluster_locations.csv", "w", newline = "") as cluster_locations_file:
		# Writer.
		cluster_sizes_writer = csv.writer(cluster_sizes_file)
		# Write the header.
		cluster_sizes_writer.writerow(["Playlist Size", "Group Size Distribution", "Shuffle", "Cluster Size", "Count"])

		# Writer.
		cluster_locations_writer = csv.writer(cluster_locations_file)
		# Write the header.
		cluster_locations_writer.writerow(["Playlist Size", "Group Size Distribution", "Shuffle", "Cluster Size", "Position", "Count"])

		# Mapping algorithms.
		maps = {
			"Balanced": rm.Balanced_Map,
			"Polacek (w = 1.0)": partial(rm.Polacek_Map, w = 1.0),
			f"Polacek (w = {args.w})": partial(rm.Polacek_Map, w = args.w),
			"Lattice": cds.Lattice_Map,
			"Spectral": cds.Spectral_Map,
			"Gaussian": cds.Gaussian_Map,
			"von Mises": cds.Von_Mises_Map,
			#"Gaussian Map (base)": gm.Gaussian_Map_Base,
			#"Gaussian Map (global)": gm.Gaussian_Map_G,
			#"Gaussian Map (local)": gm.Gaussian_Map_L,
			#"Gaussian Map (global + local)": gm.Gaussian_Map_GL,
			#"von Mises Map (base)": vmm.Von_Mises_Map_Base,
			#"von Mises Map (global)": vmm.Von_Mises_Map_G,
			#"von Mises Map (local)": vmm.Von_Mises_Map_L,
			#"von Mises Map (global + local)": vmm.Von_Mises_Map_GL,
			#"von Mises Map (base 2)": vmm.Von_Mises_Map_Base_2
		}

		# Do for all playlists.
		for size, distribution, groups in [
			("Tiny", "Impulse", impulse_t),
			("Tiny", "Uniform", uniform_t),
			("Tiny", "Binomial", binomial_t),
			("Tiny", "Zipf", zipf_t),

			("Small", "Impulse", impulse_s),
			("Small", "Uniform", uniform_s),
			("Small", "Binomial", binomial_s),
			("Small", "Zipf", zipf_s),

			("Medium", "Impulse", impulse_m),
			("Medium", "Uniform", uniform_m),
			("Medium", "Binomial", binomial_m),
			("Medium", "Zipf", zipf_m),

			("Large", "Impulse", impulse_l),
			("Large", "Uniform", uniform_l),
			("Large", "Binomial", binomial_l),
			("Large", "Zipf", zipf_l)
		]:
			print(f"{size} {distribution}")


			# Cluster size counts.
			cluster_size_counts = collections.Counter()

			# Cluster location counts.
			cluster_location_counts = collections.Counter()

			# Unbiased shuffles.
			for round in tqdm(range(args.rounds)):
				sequence = []

				# Accumulate shuffles.
				for i in range(args.n):
					shuffle = []
					# Flatten the groups.
					for group in groups:
						shuffle += group
					# Shuffle.
					random.shuffle(shuffle)
					# Append.
					sequence += shuffle

				# Run cluster metrics.
				Run_Metrics(cluster_size_counts, cluster_location_counts, sequence)

			# Write out cluster size counts.
			for length, count in cluster_size_counts.items():
				cluster_sizes_writer.writerow([size, distribution, "Unbiased", length, count])

			# Write out cluster location counts.
			for len_pos, count in cluster_location_counts.items():
				cluster_locations_writer.writerow([size, distribution, "Unbiased", len_pos[0], len_pos[1], count])



			# No Alter CD Shuffle for each map.
			for name, Map in maps.items():
				# Cluster size counts.
				cluster_size_counts = collections.Counter()

				# Cluster location counts.
				cluster_location_counts = collections.Counter()

				# CD shuffles.
				for round in tqdm(range(args.rounds)):
					sequence = []

					# Accumulate shuffles.
					for i in range(args.n):
						# Shuffle.
						shuffle = cds.Cluster_Diffusing_Shuffle(None, Map, groups)
						# Append.
						sequence += shuffle

					# Run cluster metrics.
					Run_Metrics(cluster_size_counts, cluster_location_counts, sequence)

				# Write out cluster size counts.
				for length, count in cluster_size_counts.items():
					cluster_sizes_writer.writerow([size, distribution, name, length, count])

				# Write out cluster location counts.
				for len_pos, count in cluster_location_counts.items():
					cluster_locations_writer.writerow([size, distribution, name, len_pos[0], len_pos[1], count])


"""
	Run_Metrics

	Inputs:
		(Counter)cluster_size_counts
			Cluster size counts.

		(Counter)cluster_location_counts
			Cluster location counts.

		(list)sequence
			Sequence to count cluster sizes and map cluster locations of.

	Outputs:
		None.

	Side effects:
		Modifies the cluster size and cluster location counters.
"""
def Run_Metrics(cluster_size_counts, cluster_location_counts, sequence):
	# Current run.
	run_length = 1
	# Count cluster sizes and locations.
	for i in range(1, len(sequence)):
		# Cluster continues.
		if sequence[i - 1] == sequence[i]:
			run_length += 1
		# Cluster ends.
		else:
			cluster_size_counts[run_length] += 1

			for j in range(i - run_length, i):
				cluster_location_counts[(run_length, j/len(sequence) + 1/(2*len(sequence)))] += 1

			run_length = 1

	# Count the final cluster's size and location.
	cluster_size_counts[run_length] += 1

	for j in range(len(sequence) - run_length, len(sequence)):
		cluster_location_counts[(run_length, j/len(sequence) + 1/(2*len(sequence)))] += 1


"""
	CLI.
"""
if __name__ == "__main__":
	# Argument parser.
	parser = argparse.ArgumentParser(description="Benchmark: Maps")

	# Number of rounds.
	parser.add_argument("-r", type = int, required = True, help = "Number of rounds.", dest = "rounds")

	# Number of shuffles to accumulate.
	parser.add_argument("-n", type = int, required = False, help = "Number of shuffles to accumulate.", dest = "n", default = 1)

	# w parameter for the Polacek Map.
	parser.add_argument("-w", type = float, required = False, help = "w parameter for the Polacek Map.", dest = "w", default = 0.5)

	# Pass arguments.
	main(parser.parse_args())