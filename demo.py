# Standard library.
import argparse
import math
import os
import random
from functools import partial
# CD shuffles.
import cluster_diffusing_shuffles as cds
# Reference maps.
import reference_maps as rm
# von Mises maps.
import von_mises_maps as vmm

def main(args):

	# Mapping algorithms.
	maps = {
		#"Lattice": cds.Lattice_Map,
		#"Spectral": cds.Spectral_Map,
		#"Gaussian": cds.Gaussian_Map,
		"von Mises": cds.Von_Mises_Map,
		#"Balanced": rm.Balanced_Map,
		#"Polacek (w = 1.0)": partial(rm.Polacek_Map, w = 1.0),
		#f"Polacek (w = {args.w})": partial(rm.Polacek_Map, w = args.w),
		"von Mises (σ = 1/2)": vmm.Von_Mises_Map_Base_2
	}

	os.system("color")
	groups = []
	if not args.b:
		groups = [
			[f"\033[38;2;213;166;189m{i}\033[0m" for i in range(4)],
			[f"\033[38;2;159;197;232m{i}\033[0m" for i in range(3)],
			[f"\033[38;2;182;215;168m{i}\033[0m" for i in range(2)],
			[f"\033[38;2;249;203;156m{i}\033[0m" for i in range(1)]
		]
	else:
		groups = [
			[f"\033[38;2;213;166;189m■\033[0m" for i in range(4)],
			[f"\033[38;2;159;197;232m■\033[0m" for i in range(3)],
			[f"\033[38;2;182;215;168m■\033[0m" for i in range(2)],
			[f"\033[38;2;249;203;156m■\033[0m" for i in range(1)]
		]
	"""
	# Unbiased Shuffle.
	print("\nUnbiased Shuffle\n")
	for i in range(args.r):
		sequence = []
		for j in range(args.n):
			shuffle = sum(groups, [])
			random.shuffle(shuffle)
			sequence += shuffle
		out = ""
		for item in sequence:
			out += item + " "
		print(out)
	print()
	"""
	# CD Shuffle.
	for name, Map in maps.items():
		print(f"\n{name} Shuffle\n")
		for i in range(args.r):
			sequence = []
			for j in range(args.n):
				sequence += cds.Cluster_Diffusing_Shuffle(None, Map, groups)
			out = ""
			for item in sequence:
				out += item + " "
			print(out)
		print()


"""
	CLI.
"""
if __name__ == "__main__":
	# Argument parser.
	parser = argparse.ArgumentParser(description="Spectral Shuffle")

	# Number of rounds.
	parser.add_argument("-r", type = int, required = False, help = "Number of rounds.", dest = "r", default = 1)

	# Number of shuffles to accumulate.
	parser.add_argument("-n", type = int, required = False, help = "Number of shuffles to accumulate.", dest = "n", default = 1)

	# w parameter for the Polacek Map.
	parser.add_argument("-w", type = float, required = False, help="w parameter for the Polacek Map.", dest = "w", default = 0.5)

	# Boxes?
	parser.add_argument("-b", action = "store_true", required = False, help = "Use boxes instead of squares", dest = "b")

	# Pass arguments.
	main(parser.parse_args())