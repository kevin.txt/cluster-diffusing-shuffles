# Standard library.
import collections
import heapq
import math
import random
# NumPy.
import numpy as np
# SciPy.
from scipy.stats import binom


"""
	Full_Alter

	Inputs:
		(list)group
			List of elements to alter.

	Outputs:
		None.

	Side effects:
		Alters the group's element order.
"""
def Full_Alter(group):
	random.shuffle(group)


"""
	Partial_Alter

	Inputs:
		(list)group
			List of elements to alter.

	Outputs:
		None.

	Side effects:
		Alters the group's element order.
"""
def Partial_Alter(group):
	# Alter groups with 2+ elements.
	if 1 < len(group):
		# Find the order of positions to swap.
		order = [i for i in range(0, len(group))]
		random.shuffle(order)

		# Keep track of which positions have been swapped.
		swapped = set()

		# p margin.
		# Make it so when n = 2, it's a 50% chance to swap.
		p_m = 1 - (1 - 1/np.sqrt(3/(-1 - 2/np.power(17 + 3*np.sqrt(33), 1/3) + np.power(17 + 3*np.sqrt(33), 1/3))) + np.sqrt(-2/3 + 2/(3*np.power(17 + 3*np.sqrt(33), 1/3)) - (1/3)*np.power(17 + 3*np.sqrt(33), 1/3) + 2*np.sqrt(3/(-1 - 2/np.power(17 + 3*np.sqrt(33), 1/3) + np.power(17 + 3*np.sqrt(33), 1/3)))))/2

		# Alter the elements.
		for i in order:
			# Check if position i has been swapped.
			if i not in swapped:
				# Find the n parameter.
				n = math.ceil((len(group) - 1)/4)*2

				# Find the p parameter.
				# Linear.
				p = (len(group) - 1 - i)/(len(group) - 1)
				# Sigmoid.
				#p = (math.atan(-2*math.pi*((i/(len(group) - 1)) - 1/2)))/(2*math.atan(math.pi)) + 1/2
				# Inverse Sigmoid.
				#p = (math.tan(math.atan(math.pi) - 2*(i/(len(group) - 1))*math.atan(math.pi)) + math.pi)/(2*math.pi)

				# p margin rescale.
				p = (1 - 2*p_m)*p + p_m

				# Find how many positions to move the element at position i.
				d_i = binom.rvs(n, p) - int(n/2)
				# Find the new position j.
				j = i + d_i

				# Check if position j is in bounds.
				if 0 <= j and j < len(group):
					# Check if position j has been swapped.
					if j not in swapped:
						# Swap positions i and j.
						group[i], group[j] = group[j], group[i]
						# Mark positions i and j as swapped.
						swapped.add(i)
						swapped.add(j)


"""
	Lattice_Map

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the lattice.

	Outputs:
		(list)lattice[] : (float)position

	Side effects:
		None.
"""
def Lattice_Map(n, r):
	# Create a lattice within [-r, r].
	return [(1 - n + 2*i)*r/n for i in range(n)]


"""
	Spectral_Map

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Approximate bounding radius for the spectrum.

	Outputs:
		(list)spectrum[] : (float)eigenvalue

	Side effects:
		None.
"""
def Spectral_Map(n, r):
	# Create a matrix from the Gaussian Unitary Ensemble.
	m = np.ndarray((n, n), complex)

	# Fill the lower triangle. Normalize the matrix so its spectrum is approximately within [-r, r].
	for i in range(0, n):
		# On-diagonal element.
		m[i][i] = np.random.normal(0, r/(2*np.sqrt(n)))
		# Off-diagonal elements.
		for j in range(0, i):
			m[i][j] = complex(np.random.normal(0, r/(2*np.sqrt(2*n))), np.random.normal(0, r/(2*np.sqrt(2*n))))

	# Perform spectral decomposition.
	return np.linalg.eigvalsh(m).astype(float)


"""
	Gaussian_Map

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Gaussian_Map(n, r):
	# Create a crenellation approximately within [-r, r].

	# Base.
	return sorted([(1 - n + 2*i + np.random.normal(0, 1/2))*r/n for i in range(n)])

	# Polynomial global scale.
	#return sorted([(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.normal(0, 1/2))*r/n for i in range(n)])
	# Polynomial local scale.
	#return sorted([(1 - n + 2*i + np.random.normal(0, (1/2)*(4/5 + (n - 1)/(5*n))))*r/n for i in range(n)])
	# Polynomial global + local scale.
	#return sorted([(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.normal(0, (1/2)*(4/5 + (n - 1)/(5*n))))*r/n for i in range(n)])


"""
	Von_Mises_Map

	Inputs:
		(int)n
			Number of elements to map.

		(float)r
			Bounding radius for the crenellation.

	Outputs:
		(list)positions[] : (float)position

	Side effects:
		None.
"""
def Von_Mises_Map(n, r):
	# Create a crenellation approximately within [-r, r].

	# Base.
	return [(1 - n + 2*i + np.random.vonmises(0, math.pow(2/math.pi, 2))/math.pi)*r/n for i in range(n)]

	# Polynomial global scale.
	#return [(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.vonmises(0, math.pow(2/math.pi, 2))/math.pi)*r/n for i in range(n)]
	# Polynomial local scale.
	#return [(1 - n + 2*i + np.random.vonmises(0, math.pow(2/(math.pi*((4/5) + (n - 1)/(5*n))), 2))/math.pi)*r/n for i in range(n)]
	# Polynomial global + local scale.
	#return [(1 - (n - 1)/(5*n))*(1 - n + 2*i + np.random.vonmises(0, math.pow(2/(math.pi*((4/5) + (n - 1)/(5*n))), 2))/math.pi)*r/n for i in range(n)]


"""
	Cluster_Diffusing_Shuffle

	Inputs:
		(function)Alter((list)group) : None
			An alter function which changes the element order of a list in place.

		(function)Map((int)n, (float)r) : (list)positions[] : (float)position
			A mapping function which returns a sorted list of n positions and distributed over [-r, r].

		(iterable)groups : (list)group[] : (?)element

	Outputs:
		(list)sequence

	Side effects:
		Alters each group's element order.
"""
def Cluster_Diffusing_Shuffle(Alter, Map, groups):
	# Min heap of mappings.
	heap = []

	# Find the mapping radius.
	r = max([len(group) for group in groups])

	# Alter and map each group.
	for group in groups:
			# Filter out empty groups.
			if len(group) > 0:
				# Alter the group.
				if Alter != None:
					Alter(group)

				# Map the group and append it to the heap.
				heap.append((collections.deque(Map(len(group), r)), group))

	# Restore the heap.
	heapq.heapify(heap)

	# Merge the mappings to create a sequence.
	sequence = []
	while len(heap) != 0:
		# Store the element with the most negative position between all mappings.
		sequence.append(heap[0][1][len(heap[0][1]) - len(heap[0][0])])
		# Remove that element's position from its mapping.
		heap[0][0].popleft()
		# If the element is the last of its group, remove it and restore the heap.
		if len(heap[0][0]) == 0:
			heapq.heappop(heap)
		# Otherwise, restore the heap.
		else:
			heapq.heapify(heap)

	# Return the sequence.
	return sequence
